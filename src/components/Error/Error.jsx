import React from "react";
import styled from "styled-components";

const StyledErr = styled.div`
  width:100%;
`

const Error = () => {
	return (
  <StyledErr>
    <img
      src="https://assets.prestashop2.com/sites/default/files/styles/blog_750x320/public/blog/2019/10/banner_error_404.jpg?itok=eAS4swln"
      alt=""
    />
  </StyledErr>
	)
};

export default Error;
