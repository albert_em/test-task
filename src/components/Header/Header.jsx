import React from "react";
import { Link, NavLink } from "react-router-dom";
import styled from "styled-components";
import { ReactComponent as Logo } from "../../img/header/logo.svg";
import { ReactComponent as UserIcon } from "../../img/header/user.svg";
import { menu, nav } from "../../helpers/nav-constants";

const StyledHeader = styled.header`
  height: 75px;
  background: linear-gradient(180deg, #0c174b 0%, #081440 100%);
  display: flex;
  line-height: 18.75px;
  font-size: 16px;
  background: #0c174b;
  position: fixed;
  left: 0px;
  top: 0px;
  width: 100%;
  z-index:50;
`;
const StyledUl = styled.ul`
  display: flex;
  flex: 0 1 70%;
  align-items: center;
  transition: all 0.7s ease 0s;
  & a {
    list-style-type: none;
    color: #fff;
    margin-right: 8%;
    text-decoration: none;
  }
  & a:hover {
    text-decoration: underline;
  }
`;

const AccountMenu = styled.div`
  flex: 0 0 30%;
  padding-top: 28px;
  & a {
    color: #fff;
    font-weight: 700;
    text-decoration: none;
    margin-right: 5%;
  }
`;

const StyledLogo = styled(Logo)`
  flex: 0 0 28%;
  padding: 15px 0px 0px 0px;
`;

const LoginButton = styled.button`
  border-radius: 3px;
  border: 2px solid #ae8a4d;
  width: 115px;
  height: 30px;
  color: #fff;
  font-weight: 700;
  font-size: 16px;
  background: linear-gradient(180deg, #0c174b 0%, #081440 100%);
  transition: all 0.5s ease 0s;
  &:hover {
    background: #fff;
    color: #ae8a4d;
  }
`;

const StyledLink = styled(NavLink)`
  &.active{
    font-weight: 600;
    color: gold;
  }
`;

const Header = () => {
  const renderNav = () => {
    return nav.map(({ title, link }) => (
      <StyledLink key={link} to={link} >
        {title}
      </StyledLink>
    ));
  };
  const renderAccountMenu = () => {
    return menu.map(({ title, link }) => (
      <StyledLink key={link} to={link} >
        {title}
      </StyledLink>
    ));
  };

  return (
    <StyledHeader>
      <StyledLogo>
        <Logo />
      </StyledLogo>
      <StyledUl>{renderNav()}</StyledUl>
      <AccountMenu>
        <Link to={"/account"}>
          <UserIcon />
        </Link>
        {renderAccountMenu()}
        <LoginButton>LOG IN</LoginButton>
      </AccountMenu>
    </StyledHeader>
  );
};

export default Header;
