import React from "react";
import styled from "styled-components";
import footerLogo from "../../../img/footer/Schroders.svg";

const FooterContactsContainer = styled.div`
  flex: 0 0 50%;
  color: #fff;
`;
const CompanyInfo = styled.div`
  display: flex;
  padding-top: 15px;
  align-items: center;
  margin: 0px 0px 37px 0px;
  & h5 {
    font-weight: 500;
    font-size: 18px;
    margin: 0px 0px 0px 34px;
    padding-top: 10px;
  }
`;

const CompanyAbout = styled.div`
  font-weight: 300;
  font-size: 16px;
  opacity: 0.5;
`;

const CompanyLocation = styled.div`
  display: flex;
  & p {
    margin: 0px 28px 0px 0px;
    font-size: 14px;
    
  }
`;

const FooterContacts = () => {
  return (
    <FooterContactsContainer>
      <CompanyInfo>
        <img src={footerLogo} alt="There is no logo" />
        <h5>Shroders Blended Portfolio - Fall 2019</h5>
      </CompanyInfo>
      <CompanyAbout>
        <p>Direct Investment in Alternative Assets Blockchain-based platform</p>
        <CompanyLocation>
          <p>© 2019 Shroders Blended Portfolio</p>
          <p>71-75 Shelton Street WC2H 9JQ London</p>
        </CompanyLocation>
      </CompanyAbout>
    </FooterContactsContainer>
  );
};

export default FooterContacts;
