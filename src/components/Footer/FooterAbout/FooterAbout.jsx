import { Link } from "react-router-dom";
import styled from "styled-components";
import { products } from "../../../helpers/footer-constants";

const FooterAboutContainer = styled.div`
  display: flex;
  justify-content: center;
  flex: 0 0 50%;
  color: #fff;
  padding: 25px 0px 0px 0px;
`;
const StyledProduct = styled.div`
  display: flex;
  flex-direction: column;

  & h5 {
    margin: 0px 0px 13px 0px;
  }
  & a {
    color: #fff;
    font-weight: 300;
    text-decoration: none;
    margin: 0px 0px 3px 0px;
    opacity: 0.7;
  }
  & a:hover {
    text-decoration: underline;
  }
`;

const StyledAbout = styled(StyledProduct)`
  margin: 0px 0px 0px 18%;
`;

const FooterAbout = () => {
  const renderProducts = () => {
    return products.product.map(({ name, link }) => (
      <Link key={link} to={link}>
        {name}
      </Link>
    ));
  };

  const renderAbout = () => {
    return products.about.map(({ name, link }) => (
      <Link key={link} to={link}>
        {name}
      </Link>
    ));
  };

  return (
    <FooterAboutContainer>
      <StyledProduct>
        <h5>PRODUCT</h5>
        {renderProducts()}
      </StyledProduct>
      <StyledAbout>
        <h5>ABOUT</h5>
        {renderAbout()}
      </StyledAbout>
    </FooterAboutContainer>
  );
};

export default FooterAbout;
