import React from "react";
import styled from "styled-components";
import FooterAbout from "./FooterAbout/FooterAbout";
import FooterContacts from "./FooterContacts/FooterContacts";

const StyledFooter = styled.footer`
  height: 175px;
  background: #0c174b;
  width: 100%;
`;
const FooterContainer = styled.div`
  max-width: 1100px;
  margin: 0 auto;
  display: flex;
`;

const Footer = () => {
  return (
    <StyledFooter>
      <FooterContainer>
        <FooterContacts />
        <FooterAbout/>
      </FooterContainer>
    </StyledFooter>
  );
};

export default Footer;
