import styled, { keyframes } from "styled-components";
import { ReactComponent as Loader } from "../../img/currentDeals/loader.svg";

export const fadeIn = keyframes`
  0% {
    transform: rotate(0turn);
  }

  50% {
    transform: rotate(0.5turn);
  }

  100% {
    transform: rotate(1turn);
  }
`;
export const StyledLoader = styled(Loader)`
  animation: 5s ${fadeIn} ease;
`;

export const CurrentDealsWrapper = styled.section`
  display: flex;
  justify-content: space-between;
  flex-wrap: wrap;
`;

export const PropertyCard = styled.div`
  width: calc(33%-70px)
  height: 285px;
  border-radius: 10px;
  background: #fff;
  box-shadow: 3px 9px 16px -4px rgba(0,0,0,0.5);
  margin-bottom: 35px;
`;

export const StyledHeaderCard = styled.div`
  background: #0c174b;
  border-radius: 10px 10px 0px 0px;
  color: #fff;
  h5 {
    position: absolute;
    top: 45%;
    z-index: 2;
    font-weight: 500;
    font-size: 16px;
    line-height: 19px;
    opacity: 0;
  }
  p:last-child {
    padding: 0px 20px 0px 0px;
  }
  p:first-child {
    padding: 20px 20px 0px 20px;
    font-weight: 500;
    font-size: 20px;
    margin: 0px 0px 0px 0px;
    img {
      margin: 0px 11px 0px 0px;
    }
  }
  div {
    img {
      width: 100%;
      position: relative;
    }
  }
`;

export const StyledBottomCard = styled.div`
  padding: 20px;
  color: #747474;
  div {
    display: flex;
    p:first-child {
      font-weight: 300;
      font-size: 12px;
      line-height: 14px;
    }
    p:last-child {
      margin-left: auto;
      font-weight: 500;
      font-size: 12px;
      line-height: 14px;
    }
  }
`;

export const StyledButton = styled.button`
  height: 30px;
  width: 160px;
  background: #188aae;
  border-radius: 3px;
  display: flex;
  margin: 0 auto 30px auto;
  text-align: center;
  transition: all 0.6s ease 0s;
  &:hover {
    transform: translate(1px, -10%);
    background-color: blue;
  }
  span {
    display: block;
    margin: 0 auto;
    color: #fff;
    font-weight: bold;
    font-size: 16px;
    line-height: 19px;
    padding: 4px;
  }
`;

export const LoaderContainer = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  span {
    margin-left: 10px;
    font-weight: 300;
    font-size: 18px;
    line-height: 21px;
    transition: all 0.3s ease 0s;
  }
  span:hover {
    font-weight: 600;
  }
`;
