import React, { Fragment, useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Property } from "../../helpers/properties";
import { addDeals, showMoreDeals } from "../../redux/actions/deals";

import buildImg from "../../img/currentDeals/house-icon.svg";
import houseImg from "../../img/currentDeals/property.svg";
import { ReactComponent as Loader } from "../../img/currentDeals/loader.svg";
import {
  CurrentDealsWrapper,
  LoaderContainer,
  PropertyCard,
  StyledBottomCard,
  StyledButton,
  StyledHeaderCard,
  StyledLoader,
} from "./Styles";

const createDeal = () =>
  new Property(
    "Castille et Carillon",
    187,
    "Feldman Equities",
    "16.7%",
    "1.8x",
    "10.5%",
    "$25,000",
    "8 years"
  );

const loadDeals = () => {
  const deals = [];

  for (let i = 0; i < 50; i++) {
    deals.push(createDeal());
  }

  return deals;
};

const LOADTIME = 1700;

const CurrentDeals = (props) => {
  
  const [isFetching, setIsFetching] = useState(false);

  const onClickProperty = () => {
    setIsFetching(true);

    setTimeout(() => {
      setIsFetching(false);
      dispatch(showMoreDeals());
    }, LOADTIME);
  };

  const dispatch = useDispatch();
  const property = useSelector((s) => s.deals.property);
  const showCount = useSelector((s) => s.deals.showCount);

  useEffect(() => {
    if (!property.length) {
      setIsFetching(true);

      setTimeout(() => {
        setIsFetching(false);

        const deals = loadDeals();

        dispatch(addDeals(deals));
      }, LOADTIME);
    }
  }, []);

  return (
    <Fragment>
      <CurrentDealsWrapper>
        {property
          .filter((_, i) => i + 1 <= showCount)
          .map((el, i) => (
            <PropertyCard key={`el -${i}`}>
              <StyledHeaderCard>
                <p>
                  <img src={buildImg} alt="" />
                  {el.name}
                </p>
                <span>Available shares: {el.availableShares}</span>
                <div>
                  <img src={houseImg} alt="" />
                  <h5>{el.location}</h5>
                </div>
              </StyledHeaderCard>
              <StyledBottomCard>
                <div>
                  <p>Targeted Investor IRR</p>
                  <p>{el.targetedInvestorIrr}</p>
                </div>
                <div>
                  <p>Targeted Equity Multiple</p>
                  <p>{el.targetedEquityMultiple}</p>
                </div>
                <div>
                  <p>Targeted Avarage Cash Yield</p>
                  <p>{el.targetedAvarageCashYield}</p>
                </div>
                <div>
                  <p>Distribution Commencement</p>
                  <p>{el.distributionCommencement}</p>
                </div>
                <div>
                  <p>Distribution Period</p>
                  <p>{el.distributionPeriod}</p>
                </div>
              </StyledBottomCard>
              <StyledButton>
                <span>TRADE NOW</span>
              </StyledButton>
            </PropertyCard>
          ))}
      </CurrentDealsWrapper>
      {property.length > showCount && (
        <LoaderContainer>
          {isFetching ? <StyledLoader /> : <Loader />}
          <span onClick={onClickProperty}>Show more</span>
        </LoaderContainer>
      )}
    </Fragment>
  );
};

export default CurrentDeals;
