import { DEALS_ADD, DEALS_SHOW_MORE } from "../types";

const addDeals = (deals) => ({
  type: DEALS_ADD,
  payload: deals,
});

const showMoreDeals = () => ({
  type: DEALS_SHOW_MORE,
});

export { addDeals, showMoreDeals };
