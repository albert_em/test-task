import { DEALS_ADD, DEALS_SHOW_MORE } from "../types";

const initialState = {
  property: [],
  showCount: 6,
};

const deals = (state = initialState, action) => {
  const { type, payload } = action;

  switch (type) {
    case DEALS_ADD: {
      return {
        ...state,
        property: state.property.concat(payload),
      };
    }
    case DEALS_SHOW_MORE: {
      return {
        ...state,
        showCount: state.showCount + 6,
      };
    }
    default:
      return state;
  }
};

export { deals };
