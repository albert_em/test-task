import { combineReducers } from "redux";

import { deals } from "./deals";

const rootReducer = combineReducers({
  deals,
});

export default rootReducer;
