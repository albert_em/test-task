import styled from "styled-components";
import Header from "./components/Header/Header";
import Footer from "./components/Footer/Footer";
import { Redirect, Route, Switch } from "react-router-dom";
import CurrentDeals from "./components/CurrentDeals/CurrentDeals";
import MarketPlace from "./components/MarketPlace/MarketPlace";
import Order from "./components/Order/Order";
import SellShares from "./components/SellShares/SellShares";
import Account from "./components/Account/Account";
import Error from "./components/Error/Error";

const MainWrapper = styled.section`
  margin: 0;
  padding: 0;
  font-family: "Roboto";
  box-sizing: border-box;
`;
const ContentWrapper = styled.section`
  min-height: calc(100vh - 200px);
  padding: 135px 0px 75px 0px;
  max-width: 1000px;
  margin: 0 auto;
`;
const App = () => {
  return (
    <MainWrapper>
      <Header />
      <ContentWrapper>
        <Switch>
          <Route
            exact
            path="/"
            component={() => <Redirect to="/currentDeals" />}
          />
          <Route path="/currentDeals" component={() => <CurrentDeals />} />
          <Route path="/marketPlace" component={() => <MarketPlace />} />
          <Route path="/order" component={() => <Order />} />
          <Route path="/sellShares" component={() => <SellShares />} />
          <Route path="/account" component={() => <Account />} />
          <Route component={() => <Error />} />
        </Switch>
      </ContentWrapper>
      <Footer />
    </MainWrapper>
  );
};

export default App;
