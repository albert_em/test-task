export const products = {
  product: [
    { name: "Platform", link: "/platform" },
    { name: "Use Cases", link: "/usecases" },
    { name: "Invest in Assets", link: "invest" },
    { name: "Raise capital", link: "/raisecapital" },
    { name: "Request a demo", link: "/requestademo" },
  ],
  about: [
    { name: "Company", link: "/company" },
    { name: "Events", link: "/events" },
    { name: "Blog", link: "/blog" },
    { name: "Contact Us", link: "/contacts" },
  ],
};
