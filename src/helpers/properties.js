export class Property {
  constructor(
    name,
    availableShares,
    location,
    targetedInvestorIrr,
    targetedEquityMultiple,
    targetedAvarageCashYield,
    distributionCommencement,
    distributionPeriod
  ) {
    this.name = name;
    this.availableShares = availableShares;
    this.location = location;
    this.targetedInvestorIrr = targetedInvestorIrr;
    this.targetedEquityMultiple = targetedEquityMultiple;
    this.targetedAvarageCashYield = targetedAvarageCashYield;
    this.distributionCommencement = distributionCommencement;
    this.distributionPeriod = distributionPeriod;
  }
}
