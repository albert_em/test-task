export const nav = [
  { title: "Current Deals", link: "/currentDeals" },
  { title: "Marketplace", link: "/marketPlace" },
  { title: "Order", link: "/order" },
  { title: "Sell Shares", link: "/sellShares" },
];

export const menu = [
  { title: "JOIN", link: "/join" },
];
